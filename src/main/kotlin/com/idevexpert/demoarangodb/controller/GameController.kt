package com.idevexpert.demoarangodb.controller

import com.idevexpert.demoarangodb.bean.request.CreateGameRequest
import com.idevexpert.demoarangodb.bean.response.CreateGameResponse
import com.idevexpert.demoarangodb.persistence.GameRepository
import com.idevexpert.demoarangodb.persistence.entity.Game
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/game")
class GameController(
        val gameRepository: GameRepository
) {
    @PostMapping
    fun createGame(
            @RequestParam("nombre") nombre: String,
            @RequestParam("rating") rating: Double
    ) : ResponseEntity<String> {
        val gaaaa = Game(
                nombre = nombre,
                rating = rating
        )
        gameRepository.save(gaaaa)

        return ResponseEntity("OK", HttpStatus.OK)
    }

    @PostMapping("/createByJson")
    fun createGameJson(
            @RequestBody nuevoObjeto: CreateGameRequest
    ) : ResponseEntity<CreateGameResponse> {
        val gaaaaaaaaaaaaa = Game(
                nombre = nuevoObjeto.nombre,
                rating = nuevoObjeto.rating
        )

        gameRepository.save(gaaaaaaaaaaaaa)

        return ResponseEntity(CreateGameResponse(
                success = true
        ), HttpStatus.OK)
    }

    @GetMapping
    fun getGames() : ResponseEntity<List<Game>> {
        val gamesList = arrayListOf<Game>()
        gameRepository.findAll().forEach { gamesList.add(it) }
        return ResponseEntity(gamesList, HttpStatus.OK)
    }

}