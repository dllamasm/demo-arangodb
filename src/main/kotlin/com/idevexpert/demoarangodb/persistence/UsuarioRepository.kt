package com.idevexpert.demoarangodb.persistence

import com.arangodb.springframework.repository.ArangoRepository
import com.idevexpert.demoarangodb.persistence.entity.Usuario

interface UsuarioRepository : ArangoRepository<Usuario, String>  {
    fun findByUsername(username: String) : Usuario
    fun findByUsernameAndPassword(username: String, password: String) : Usuario
}