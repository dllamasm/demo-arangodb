package com.idevexpert.demoarangodb.persistence

import com.arangodb.springframework.repository.ArangoRepository
import com.idevexpert.demoarangodb.persistence.entity.Game

interface GameRepository : ArangoRepository<Game, String> {

}