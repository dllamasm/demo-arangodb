package com.idevexpert.demoarangodb.persistence.entity

import com.arangodb.springframework.annotation.Document
import org.springframework.data.annotation.Id
import java.time.LocalDateTime

@Document("usuarios")
data class Usuario(
    @Id val id: String? = null, // Cuando es NUEVO, No tiene "ID" -> INSERT

    val foto: ByteArray? = null,
    val username: String,
    var password: String,
    var email: String,

    val fechaDeCreacion: LocalDateTime = LocalDateTime.now(),
    var fechaDeModificacion: LocalDateTime = LocalDateTime.now()
    ) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Usuario

        if (id != other.id) return false
        if (foto != null) {
            if (other.foto == null) return false
            if (!foto.contentEquals(other.foto)) return false
        } else if (other.foto != null) return false
        if (username != other.username) return false
        if (password != other.password) return false
        if (email != other.email) return false
        if (fechaDeCreacion != other.fechaDeCreacion) return false
        if (fechaDeModificacion != other.fechaDeModificacion) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (foto?.contentHashCode() ?: 0)
        result = 31 * result + username.hashCode()
        result = 31 * result + password.hashCode()
        result = 31 * result + email.hashCode()
        result = 31 * result + fechaDeCreacion.hashCode()
        result = 31 * result + fechaDeModificacion.hashCode()
        return result
    }
}