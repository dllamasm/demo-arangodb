package com.idevexpert.demoarangodb.persistence

import com.arangodb.springframework.repository.ArangoRepository
import com.idevexpert.demoarangodb.persistence.entity.Expediente

interface ExpedienteRepository : ArangoRepository<Expediente, String> {

    fun findByNroExpediente(nroExpediente: String): Expediente
    fun findAllByAsuntoIsLike(asuntoIsLike: String): List<Expediente>

    fun findAllByCreador_Id(creadorId: String): List<Expediente>
    fun findAllByCreador_Username(creadorUserName: String): List<Expediente>

}